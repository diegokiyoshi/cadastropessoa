package br.diego;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class CadastroPessoaSimples {

	public static void main(String[] args) {
		
		Scanner scanner = new Scanner(System.in);
		List<Pessoa> listaPessoasCadastradas = new ArrayList<Pessoa>();
		
		Boolean desejaCadastrar = Boolean.TRUE;
		while(desejaCadastrar) {
			System.out.println("Por favor, digite o nome da pessoa a ser cadastrada ou SAIR");
			String nomePessoaCadastrada = scanner.nextLine();
			
			if(!nomePessoaCadastrada.equalsIgnoreCase("SAIR")) {
				Pessoa pessoa = new Pessoa();
				pessoa.setNome(nomePessoaCadastrada);
				listaPessoasCadastradas.add(pessoa);
				
			}else {
				desejaCadastrar = Boolean.FALSE;
			}
		}
		
		//Pessoas:Diogo, Jo�o, Pedro, Emanuela.
		
		System.out.print("Pessoas:");
		for (Pessoa pessoa : listaPessoasCadastradas) {
			
			if(listaPessoasCadastradas.size()-1 == listaPessoasCadastradas.indexOf(pessoa)) {
				System.out.println(pessoa.getNome()+".");
			} else {
				System.out.print(pessoa.getNome() + ", ");
			}
				
			
		}
		
	}
}
